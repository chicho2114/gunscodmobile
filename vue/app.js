Vue.use(VueI18n);

// Ready translated locale messages
const messages = {
  en: {
    title: 'Guns Call of Duty Mobile',
    buttonLanguage: 'Ver en Español',
    lastUpdate: 'Last Update',
    descriptionWebSite: 'Weapon statistics of the videogame Call of Duty Mobile 2021 updated',
    menu: {
      tab1: 'General Description',
      tab2: 'Comparison of all weapons (COMING SOON)',
      tab3: 'Weapons laboratory (COMING SOON)',
    },
    buttonFilter: 'Filter',
    titleFilter: 'Filter by the name of the weapon',
    labelFilter: 'Enter the name of the weapon',
    messageComingSoon: 'COMING SOON',
    stats: {
        damage: 'damage',
        firerate: 'firerate',
        accuracy: 'accuracy',
        mobility: 'mobility',
        range: 'range',
        control: 'control',
    },
    seeStats: 'See Stats',
    feature: 'Feature',
    buttonReturnUp: 'Return to top',
    developedBy: 'Developed by',
    copyRight1: 'Each of the logos, images, names and brands belong to each of their respective owners.',
    copyRight2: 'This site only shows the attributes for each of the weapons in the Call of Duty Mobile video game. '
  },
  es: {
    title: 'Armas Call of Duty Mobile ',
    buttonLanguage: 'See in English',
    lastUpdate: 'Última Actualización',
    descriptionWebSite: 'Estadísticas de armas de Call of Duty Mobile 2021 Actualizadas',
    menu: {
      tab1: 'Descripcion General',
      tab2: 'Comparación de todas las armas (PRÓXIMAMENTE)',
      tab3: 'Laboratorio de armas (PRÓXIMAMENTE)',
    },
    buttonFilter: 'Filtrar',
    titleFilter: 'Filtrar por nombre del arma',
    labelFilter: 'Introduzca nombre del arma',
    messageComingSoon: 'DISPONIBLE PRÓXIMAMENTE',
    stats: {
        damage: 'daño',
        firerate: 'cadencia',
        accuracy: 'precisión',
        mobility: 'movilidad',
        range: 'alcance',
        control: 'control',
    },
    seeStats: 'Ver Características',
    feature: 'Característica',
    buttonReturnUp: 'Regresar Arriba',
    developedBy: 'Desarrollado por',
    copyRight1: 'Cada uno de los logos, imagenes, nombres y marcas pertenecen a cada uno de sus respectivos dueños.',
    copyRight2: 'Este sitio solo muestra los atributos para cada una de las armas del videojuego Call of Duty Mobile. '
  }
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'es',
  messages, // set locale messages
})

var app = new Vue({ 
    i18n,
    el: '#app',
    data: {
    	dateUpdate: '05-09-2021',
        word: "",
        option: "",
        classButton: "",
        classActive: "active",
        guns: [
        	  	

        ],
        gunsDamage: [

        ],
        gunsFirerate: [

        ],
        gunsAccuracy: [

        ],
        gunsMobility: [

        ],
        gunsRange: [

        ],
        gunsControl: [

        ],
    },
    methods:{
        changeLanguage: function () {
            i18n.locale == 'en' ? i18n.locale = 'es' : i18n.locale = 'en';
        },
        typeGun: function (type){
            switch (type){
                case "RIFLE DE ASALTO":

                    return i18n.locale == 'es' ? type : 'ASSAULT RIFLE';

                break;
                case "AMETRALLADORA LIGERA":

                    return i18n.locale == 'es' ? type : 'LIGHT MACHINE GUN';

                break;
                case "ESCOPETA":

                    return i18n.locale == 'es' ? type : 'SHOTGUN';

                break;
                case "SUBFUSIL":

                    return i18n.locale == 'es' ? type : 'SUB MACHINE GUN';

                break;
                case "PISTOLA":

                    return i18n.locale == 'es' ? type : 'PISTOL';

                break;
                case "LANZADOR":

                    return i18n.locale == 'es' ? type : 'LAUNCHER';

                break;
                case "TIRADOR":

                    return i18n.locale == 'es' ? type : 'MARKSMAN';

                break;
                case "CUERPO A CUERPO":

                    return i18n.locale == 'es' ? type : 'MELEE WEAPONS';

                break;

                default:
                    return type;
            }
        }
    },
    computed: {
    	filterGuns: function () {
    		const search = String(this.word).toUpperCase();

            if (this.option === "damage") return this.guns.sort((a,b) => {
                return b.damage - a.damage
            });
            if (this.option === "firerate") return this.guns.sort((a,b) => {
                return b.firerate - a.firerate
            });
            if (this.option === "accuracy") return this.guns.sort((a,b) => {
                return b.accuracy - a.accuracy
            });
            if (this.option === "mobility") return this.guns.sort((a,b) => {
                return b.mobility - a.mobility
            });
            if (this.option === "range") return this.guns.sort((a,b) => {
                return b.range - a.range
            });
            if (this.option === "control") return this.guns.sort((a,b) => {
                return b.control - a.control
            });

            if (search) return this.guns.filter(c => c.name.indexOf(search) > -1);

    		return this.guns.filter(c => c.name.indexOf(search) > -1);
    	}
    },
    created: function () {
         const dbRef = firebase.database().ref();
        dbRef.child("guns").get().then((snapshot) => {
          if (snapshot.exists()) {
            this.guns = snapshot.val();

            this.gunsDamage = [...this.guns];
            this.gunsFirerate = [...this.guns];
            this.gunsAccuracy = [...this.guns];
            this.gunsMobility = [...this.guns];
            this.gunsRange = [...this.guns];
            this.gunsControl = [...this.guns];

            this.gunsDamage = this.gunsDamage.sort((a,b) => {
                return b.damage - a.damage
            });

            this.gunsFirerate = this.gunsFirerate.sort((a,b) => {
                return b.firerate - a.firerate
            });

            this.gunsAccuracy = this.gunsAccuracy.sort((a,b) => {
                return b.accuracy - a.accuracy
            });

            this.gunsMobility = this.gunsMobility.sort((a,b) => {
                return b.mobility - a.mobility
            });

            this.gunsRange = this.gunsRange.sort((a,b) => {
                return b.range - a.range
            });

            this.gunsControl = this.gunsControl.sort((a,b) => {
                return b.control - a.control
            });

          } else {
            alert("No data available");
            console.log(snapshot.val());
          }
        }).catch((error) => {
          console.error(error);
        });

        //const StRef = firebase.storage().ref();
        
        //StRef.listAll();
            //console.log(StRef.listAll());
            //console.log(StRef);
          
    }
});